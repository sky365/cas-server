package com.chinecredit.cas.domain;

import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.jasig.cas.authentication.UsernamePasswordCredential;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Objects;

public class UsernamePasswordAndKaptchaCredential extends UsernamePasswordCredential {

    @NotNull
    @Size(min = 5,message = "authenticationFailure.NullDataException")
    private String kaptcha;

    public String getKaptcha() {
        return kaptcha;
    }

    public void setKaptcha(@NotNull String kaptcha) {
        this.kaptcha = kaptcha;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        UsernamePasswordAndKaptchaCredential that = (UsernamePasswordAndKaptchaCredential) o;
        if(super.getUsername() == null || "".equals(super.getUsername())
                || super.getPassword() == null || "".equals(super.getPassword())
                || this.kaptcha == null || "".equals(this.kaptcha)){
            return false;
        }
        return super.getUsername().equals(that.getUsername()) && super.getPassword().equals(that.getPassword()) && this.kaptcha.equals(that.getKaptcha());
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(super.getUsername()).append(super.getPassword()).append(this.getKaptcha()).toHashCode();
    }
}
