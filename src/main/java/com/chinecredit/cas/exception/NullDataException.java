package com.chinecredit.cas.exception;

import org.jasig.cas.authentication.RootCasException;

public class NullDataException extends RootCasException {
    public static final String CODE = "authenticationFailure.NullDataException";

    public NullDataException(){
        super(CODE);
    }
}
