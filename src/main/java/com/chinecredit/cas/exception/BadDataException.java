package com.chinecredit.cas.exception;

import org.jasig.cas.authentication.RootCasException;

public class BadDataException extends RootCasException {
    public static final String CODE = "authenticationFailure.BadDataException";

    public BadDataException(){
        super(CODE);
    }
}
