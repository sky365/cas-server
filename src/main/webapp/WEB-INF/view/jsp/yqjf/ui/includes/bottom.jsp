<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<footer class="footer text-center">
    <p class="text">
        <span><spring:message code="screen.copyright.zbdw" /></span>
        <span><spring:message code="screen.copyright.cbdw" /></span>
        <span><spring:message code="screen.copyright.ywzx" /></span>
        <span><spring:message code="screen.copyright.jszx" /></span>
    </p>
</footer>

</div> <!-- END #container -->
<spring:theme code="cas.javascript.file" var="casJavascriptFile" />
<spring:theme code="cas.custom.javascript.file" var="casCustomJavascriptFile" />
<script type="text/javascript" src="<c:url value="${casJavascriptFile}" />"></script>
<script type="text/javascript" src="<c:url value="${casCustomJavascriptFile}" />"></script>
</body>
</html>

