<jsp:directive.include file="includes/top.jsp" />
<div id="content" class="login-body">
    <!-- END #content -->
    <div class="content-in">
    <div id="cookiesDisabled" class="errors" style="display:none;">
        <h2><spring:message code="screen.cookies.disabled.title" /></h2>
        <p><spring:message code="screen.cookies.disabled.message" /></p>
    </div>

    <img class="login-text" src="/sso/themes/yqjf/images/login_text.png" />
    <div class="login-page" id="login">

        <form:form method="post" id="fm1" commandName="${commandName}" htmlEscape="true" class="el-form">

            <h2 class="color1 font21 login-form-title"><spring:message code="screen.welcome.instructions" /></h2>

            <div class="row login-list">
                <form:errors path="*" id="msg" cssClass="error-msg" element="div" htmlEscape="false" />
            </div>

            <section class="row login-list">
                <div class="row">
                    <c:choose>
                        <c:when test="${not empty sessionScope.openIdLocalId}">
                            <strong><c:out value="${sessionScope.openIdLocalId}" /></strong>
                            <input type="hidden" id="username" name="username" class="el-input" value="<c:out value="${sessionScope.openIdLocalId}" />" />
                        </c:when>
                        <c:otherwise>
                            <spring:message code="screen.welcome.label.netid.accesskey" var="userNameAccessKey" />
                            <spring:message code="screen.welcome.label.netid" var="userNameLabel" />
                            <form:input cssClass="required" cssErrorClass="error" id="username" size="25" tabindex="1" placeholder="${userNameLabel}" accesskey="${userNameAccessKey}" path="username" autocomplete="off" htmlEscape="true" />
                        </c:otherwise>
                    </c:choose>
                </div>
            </section>

            <section class="row login-list">
                <spring:message code="screen.welcome.label.password.accesskey" var="passwordAccessKey"  />
                <spring:message code="screen.welcome.label.password" var="userPassword" />
                <form:password cssClass="required" cssErrorClass="error" id="password" class="el-input" size="25" tabindex="2" placeholder="${userPassword}" path="password"  accesskey="${passwordAccessKey}" htmlEscape="true" autocomplete="off" />
                <span id="capslock-on" style="display:none;"><p><img src="images/warning.png" valign="top"> <spring:message code="screen.capslock.on" /></p></span>
            </section>

            <section class="row login-vcode login-list">
                <spring:message code="screen.welcome.label.kaptcha.accesskey" var="kaptchaAccessKey" />
                <spring:message code="screen.welcome.label.kaptcha" var="userKaptcha" />
                <form:input cssClass="required" class="login-vcode" cssErrorClass="error" id="kaptcha" size="5" tabindex="3" placeholder="${userKaptcha}" accesskey="${kaptchaAccessKey}" path="kaptcha" autocomplete="off" htmlEscape="true" />
                <img src="captcha.jpg" onclick="this.src = 'captcha.jpg?t=' + new Date().getTime();" class="marL10 pointer">
            </section>

            <section class="row login-list">
                <input type="checkbox" value="true" id="rememberMe" name="rememberMe" />
                <spring:message code="screen.welcome.label.rememberme" />
            </section>

            <section class="row btn-row clearBoth login-list">
                <input type="hidden" name="lt" value="${loginTicket}" />
                <input type="hidden" name="execution" value="${flowExecutionKey}" />
                <input type="hidden" name="_eventId" value="submit" />

                <input class="btn-submit login-btn fl" name="submit" accesskey="l" value="<spring:message code="screen.welcome.button.login" />" tabindex="6" type="submit" />
               <%-- <input class="btn-reset login-btn fl" name="reset" accesskey="c" value="<spring:message code="screen.welcome.button.clear" />" tabindex="7" type="reset" />--%>
            </section>
            <section class="row login-list last-row">
                <div class="text-center login-register color2">
                    <spring:theme code="cas.custom.register.url" var="registerUrl" />
                    <spring:theme code="cas.custom.register.noaccount" />
                    <a class="color1" href="${registerUrl}">
                        <spring:theme code="cas.custom.register.label" />
                    </a>
                </div>
            </section>
        </form:form>
    </div>
</div>
</div>
<jsp:directive.include file="includes/bottom.jsp" />
