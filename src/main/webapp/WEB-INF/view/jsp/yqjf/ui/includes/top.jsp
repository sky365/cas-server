<!DOCTYPE html>

<%@ page pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html lang="en">
<head>
  <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
  
    <title><spring:message code="screen.yqjf.title" /> </title>

    <spring:theme code="cas.custom.css.file" var="customCssFile" />
    <spring:theme code="cas.custom.favicon.file" var="customFaviconFile" />
    <link rel="stylesheet" href="<c:url value="${customCssFile}" />" />
    <link rel="icon" href="<c:url value="${customFaviconFile}" />" type="image/x-icon" />
</head>
<body id="cas">
  <div id="container">
      <header class="header">
          <div class="header-body">
              <spring:theme code="cas.custom.logo.url" var="indexUrl" />
              <a href="${indexUrl}" class="logo-yqjf"></a>
          </div>
      </header>
