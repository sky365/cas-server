<jsp:directive.include file="includes/top.jsp" />
<div id="content" class="login-body">
  <div id="msg" class="success">
    <div class="login-success">
      <h2><spring:message code="screen.success.header" /></h2>
      <p><spring:message code="screen.success.success" arguments="${principal.id}"/></p>
      <p><spring:message code="screen.success.security" /></p>
      <div class="go-login">
        <a class="login-btn return-login" href="/logout"><spring:message code="screen.welcome.label.gologout" /></a>
      </div>
    </div>
  </div>
</div>
<jsp:directive.include file="includes/bottom.jsp" />

